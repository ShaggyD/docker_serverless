FROM node:8.11.1-stretch
MAINTAINER me@dchadwick.com # Based on efforts from marklkelly@gmail.com

# Build Arguments
ARG BUILD_ID=unknown
ARG GIT_BRANCH=unknown
ARG GIT_COMMIT=unknown
ARG GIT_TAG=unknown
ARG SERVERLESS_VERSION=unknown
ENV BUILD_ID $BUILD_ID
ENV GIT_BRANCH $GIT_BRANCH
ENV GIT_COMMIT $GIT_COMMIT
ENV GIT_TAG $GIT_TAG
ENV SERVERLESS_VERSION $SERVERLESS_VERSION

# Create the directory to run projects from
RUN mkdir -p /home/projects

#Install required applications
RUN apt-get update && apt-get install -y unzip python python-dev python-pip python3 python3-dev python3-pip && apt-get clean

# Include AWS CLI
#RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" && \
#    unzip awscli-bundle.zip &&\
#    ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
RUN pip install awscli

# Upgrade to python 3.6 support
RUN echo "deb http://ftp.de.debian.org/debian testing main" >> /etc/apt/sources.list
RUN echo 'APT::Default-Release "stable";' >> /etc/apt/apt.conf.d/00local
RUN apt-get update && apt-get -y -t testing install python3.6 && apt-get clean

# Install Serverless
RUN npm install -g serverless@$SERVERLESS_VERSION
